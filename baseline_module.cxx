/******************************************************************
 * ADC Baseline calculation *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <numeric>
#include <functional>

#include "json.hpp"
using json = nlohmann::json;


class BaselineFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class BaselineModule: public TARunObject
{
public:
   BaselineFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors
   int nbase_samples;

public:
   BaselineModule(TARunInfo* runinfo, BaselineFlags* f): TARunObject(runinfo),
                                                         fFlags(f), fCounter(0), fError(0)
   {
      if(fFlags->fVerbose) std::cout<<"BaselineModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Baseline";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"BaselineModule Json parsing success!"<<std::endl;
      fin.close();

      nbase_samples=settings["Baseline"]["Pedestal"].get<int>();
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"BaselineModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"BaselineModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"BaselineModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSAdcEventFlow* adc_flow = flow->Find<DSAdcEventFlow>();
      if( !adc_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( !adc_flow->getNchannels() ) return flow;
      if( fFlags->fVerbose )
         std::cout<<"BaselineModule::AnalyzeFlowEvent # of channels: "
                  <<adc_flow->getNchannels()
                  <<", # of samples per channel: "<<adc_flow->getNsamples(0)<<std::endl;
      else if( (adc_flow->getEventNumber()%1000)==0 )
         std::cout<<"AnalyzeFlowEvent #"<<std::setw(8)<<adc_flow->getEventNumber()<<std::endl;
      if( adc_flow->getNsamples(0) < nbase_samples )
         {
            std::cerr<<"BaselineModule::AnalyzeFlowEvent pedestal length exceeds number of samples"<<std::endl;
            return flow;
         }
         
      DSProcessorFlow* wf_flow = new DSProcessorFlow(flow);
      wf_flow->conf[adc_flow->ftype[0]].fNSamples=adc_flow->getNsamples(0);
      wf_flow->fEventNumber=adc_flow->getEventNumber();
      
      for(int n=0; n<adc_flow->getNchannels(); ++n)
         {
            if( fFlags->fVerbose )
               std::cout<<"BaselineModule:: "<<std::setw(4)<<n<<") "<<adc_flow->ftype[n]
                        <<" board: "<<adc_flow->getBoard(n)
                        <<" ch: "<<adc_flow->getChannelNumber(n)
                        <<" idx: "<<adc_flow->getChannelIndex(n)
                        <<" samples: "<<adc_flow->getNsamples(n);

            // Create channel object 
            TDSChannel dschan( adc_flow->getBoard(n), adc_flow->getChannelNumber(n),
                               adc_flow->ftype.at(n).c_str() );

            // obtain the waveform, we will not modify it
            const std::vector<double>* adc_samples = adc_flow->getVector(n);

            double baseline=std::accumulate(adc_samples->begin(),
                                            adc_samples->begin()+nbase_samples,
                                            double(0))/(double) nbase_samples;
            double baseline_rms=std::inner_product(adc_samples->begin(),
                                                   adc_samples->begin()+nbase_samples,
                                                   adc_samples->begin(),double(0));
            baseline_rms = sqrt( (baseline_rms / (double) nbase_samples)
                                 - (baseline * baseline) );
           
            if( fFlags->fVerbose )
               std::cout<<" baseline: "<<baseline<<" RMS: "<<baseline_rms<<std::endl;

            // calculate the standard deviation of the whole waveform
            // using the mean baseline of the fist nbase_samples
            double number_of_samples = (double) adc_samples->size();
            double wf_rms=std::inner_product(adc_samples->begin(),
                                             adc_samples->end(),
                                             adc_samples->begin(),double(0));
            wf_rms=sqrt( (wf_rms / number_of_samples) - (baseline * baseline) );
               
            // Store results
            dschan.baseline = baseline;
            dschan.baseline_rms = baseline_rms;
            dschan.wf_rms = wf_rms;

            // Copy unfiltered waveform in the TDSChannel structure
            // after baseline subtraction and make it "positive"
            dschan.unfiltered_wf.resize(adc_samples->size());
            std::transform(adc_samples->begin(),adc_samples->end(),
                           dschan.unfiltered_wf.begin(),
                           [baseline](double y){return -1.*(y-baseline);});

            // get maximum amplitude of waveform
            dschan.maxph=*std::max_element(dschan.unfiltered_wf.begin(),dschan.unfiltered_wf.end());
           
            wf_flow->channels->push_back(dschan);
         }// channels loop
         if( fFlags->fVerbose )
               std::cout << "BaselineModule:: End Channels Loop" << std::endl;
      flow = wf_flow;
      ++fCounter;
      return flow;
   }

};


class BaselineModuleFactory: public TAFactory
{
public:
   BaselineFlags fFlags;
   
public:
   void Usage()
   {
      //      printf("Modules options flags:\n");
      printf("\n");
      printf("\t--config, --conf, -c </path/to/json_file>\n\t\tspecify path json configuration file\n");
      printf("\t--verbose, -v\n\t\tprint status information\n");
      printf("\t--skip-events, -s <number>\n\t\tskip to event <number> (starting from 0)\n");
   }

   void Init(const std::vector<std::string> &args)
   {
      printf("BaselineModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("BaselineModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("BaselineModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new BaselineModule(runinfo, &fFlags);
   }

};

static TARegister tar __attribute__((init_priority(103))) (new BaselineModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
