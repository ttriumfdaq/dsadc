# DarkSide ADCs analyzer #

This analyzer wants to provide a unified interface to process data from the CAEN V1725 ADC,
used for the CERN proto-0 and from the CAEN VX2740.

This analyzer employs the [manalyzer](https://bitbucket.org/tmidas/manalyzer) framework, 
which allows a clever modularization of the analysis task, a powerful multithreading 
implementation and a code profiler.

## Prerequisites ##

* [ROOT](https://github.com/root-project/root), tested with 6.22/08
	- available through `ROOTSYS`
* [MIDAS](https://bitbucket.org/tmidas/midas), tested with `2d3872df`
	- available through `MIDASSYS`
	- it also provides [manalyzer](https://bitbucket.org/tmidas/manalyzer) as a submodule
* [CMake](www.cmake.org), require version >= 3.12


## Build ##


```
mkdir build && cd build
cmake ..
cmake --build . --target install -- -j
```

The executable `dsanana.exe` is in the `bin` directory.


### CMake build Boolean options ###

`-DV1725=ON`    Enable CAEN V1725 ADC data unpack module

`-DVX2740=OFF`  Disable CAEN VX2740 ADC data unpack module

`-DFILTER=ON`   Enable waveform filtering module

`-DPLOT=ON`     Enable waveform export

`-DHISTO=ON`    Enable pulse histogramming module

`-DTTREE=OFF`   Disable ROOT output


Different combinations of these variables produce different executables: this is a convenient customization scheme.



## Run ##

Interactive mode with JSROOT server and live canvases (requires X11).

```
dsanana.exe -R8885 -Ooutput.root -i -g -- --verbose
```

By default the settings are read from [`master.json`](master.json) thanks to [nlohmann/json](https://github.com/nlohmann/json). The config file is written to the rootfile in `treewriter_module`.



Batch mode with ROOT output

```
dsanana.exe --mt /path/to/midasfile/run0XYZsub*.mid[.lz4] -- --config my_test.json
```

By default the rootfile is created in `./root_output_files` as `output0XYZ.root`.




### Useful CLI options ###


`-i` allows to navigate one event at the time (Interactive module)

`-g` enables graphical display

`--mt`: Enable multithreaded mode

`-Ooutputfile.root`: Specify output root file filename

`--`: All following arguments are passed to the analyzer modules

`--verbose/-v` print to stdout what's happening

`--config/--conf/-c </path/to/json>` set path to configuration file

`--skip-events/-s <event number>` jump to the event specified



## Code Information ##

Most of the code in this repo has been reused from the [dsproto_analyzer](https://bitbucket.org/ttriumfdaq/dsproto_analyzer). Specifically the VX2740 and V1725 data decoder are copied verbatim, including their inheritance from `TGenericData` (see [here](https://bitbucket.org/tmidas/rootana/src/master/libAnalyzer/TGenericData.hxx)).

The raw data per channel now inherit from the base class `TRawChannel` in order to provide a unified interface to access the ADC samples.

The high-level data structures `TDSChannel` and `TDSPulse` are copied in `dsdata.hxx` with the addition of a new member to identify the type of ADC. These objects are used in the data flow `TAFlowEvent`.

The reconstructed pulse's time, amplitude and charge is stored in a `TTree`, named `TPulse`, along with the channel index.

The beginning and the end of the run timestamps are saved to the output root file.

Any other additional ODB variable can be written to disk following the example provided by `historywriter_NA-setup-1_module.cxx`.


### JSON configuration file ###

The [JSON parser](json.hpp) is provided by [nlohmann/json](https://github.com/nlohmann/json).

The meaning of the keys of the default configuration [file](master.json) is the following:

* `Baseline/Pedestal` allows the user to set the number of samples used to calculate the baseline for pulse processing. The same value is used to calculate the pedestal before the ROI


* `Filter/Name` identifies the filtering strategy. Available filters are:
  
	- `Moving Average`, default, with 100 samples window.
  
	- `Exponential`, with smoothing parameter in (0,1).
  
	- `ARMA`, with "tau" specified by `Par0`, "sigma" by `Par1` and the "expontial ratio" by `Par2`.

* `Filter/Par<n>` set the n-th parameter used in the filter


* `ROI` allows the user to set a "Region Of Interest" to extract amplitude and charge, without pulse processing


* `Pulse/Fixed` if `true` the pulses are identified using a fixed threshold procedure, otherwise the default strategy is the calculate the height of the signal in terms on baseline rms.

* `Pulse/Sigma` number of sigma (floating point) above the baseline rms to identify a pulse

* `Pulse/Amplitude` fixed threshold for pulse finding

* `Pulse/Duration` minimum length required for a pulse, helps to get rid of noise when minimal filtering is employed

* `Pulse/Charge` allows the user to set the integration window around the found pulse. `before` is the number of samples before the leading edge of the pulse (lower integration limit). `total` is the total integration window in samples.


* `Plot/Canvas` shows a live canvas with the first 16 processed channels and another one with `Plot/Channel`

* `Plot/Persistency` enables the sum of waveforms of all events until `Persistency Events Limit` in each channel. If `Plot/Canvas`, a canvas with channel `Persistency Channel` is also shown and creates a PDF at the end of the run.

* `Plot/Save PDF` print to PDF the canvases up to the `Save Events Limit` event.


The JSON configuration file is save to the output ROOT file in the `treewriter_module`.


### ROOT Output ###

`TChan` is a `TTree` structured around the ADC channels. The branches are the following:

* `index` is the channel number according to the formula N x k + I, where N is an index running on the ADC modules, k is the number of (active) channels per ADC and I is the number of channels per ADC module. Both N and I are assumed to start from "0".

* `baseline` and `noise` are, respectively, the average signal level in ADC and its root-mean-square. This average is over a number of samples specified in the configuration file.

* `maxph` is the maximum amplitude (peak-to-baseline) seen in a waveform.

* `filtbaseline` and `filtnoise` are the same as `baseline` and `noise`, respectively, but calculated on the filtered waveform.

* `roiq`, `roih` and `roit` are the "charge", the pulse height and time of the waveform in the ROI, specified in the configuration file.

* `filtroiq`, `filtroih` and `filtroit` are the same as `roiq`, `roih` and `roit`, respectively, but calculated on the filtered waveform. The boundaries of the filtered-ROI can be different from the unfiltered one.

* `wfrms` and `filtwfrms` are the baseline standard deviation for the raw and the filtered waveform, respectively. Here the mean baseline is calculated over the first samples, as set by `Baseline/Pedestal`.

* `nchan` is the total number of channels read out.

* `mts` is the MIDAS time stamp, i.e., Unix time.

* `eventcounter`, depending on the configuration of the ADC, it's the number of all triggers.

* `trigtime`, it represents the time in seconds of the trigger since the start of the readout.

* `Nevent` indicates the progressive event number.

Except for `nchan`, `mts`, `eventcounter` and `Nevent`, that are scalars, the other branches store `std::vector<double>`, where each entry is a channel.


`TPulse` is a data structure based on the pulse finder. In addition to `index`, `nchan`, `mts`, `eventcounter`, `trigtime` and `Nevent`, that represent the same quantities described above, the other branches are the following:

* `charge` is the integral over a fixed range, specified in the configuration file, around the found pulse.

* `time` is the time in "samples" of the threshold-crossing pulse. If the ARMA filter is used, this `time` is the average around the peak position.

* `height` is the pulse height in ADC.

* `ToT` is the time over threshold of the pulse.

* `npulse` indicates the number of pulses found in the current event.

Except for `npulse`, `mts`, `eventcounter`, `trigtime` and `Nevent`, that are scalars, the other branches store `std::vector<double>`, where each entry is a pulse.


### Colour-coded Graphic Output ###

* Waveforms
     - Raw: black
     - Baseline subtracted: blue
     - Filtered: red
     - Found pulse: light green

* ROI decoration in baseline subtracted waveform
     - lower limit: black solid line
     - upper limit amplitude: black fine-dash line
     - upper limit charge: black long-dash line
 
* ROI decoration in filtered waveform
     - lower limit: blue solid line
     - upper limit amplitude: blue fine-dash line
     - upper limit charge: blue long-dash line



### Performance ###

See [here](INFO.md)
