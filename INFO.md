INFO.md for

commit efc155ec0205ec5b000f1f16a1e04322af48e1c9 (HEAD -> master, tag: v1.0, origin/master)

# Memcheck, a memory error detector

Using Valgrind-3.13.0 and LibVEX; rerun with -h for copyright info
Command: `dsanana.exe -e2000 -O/home/dsdaq/andrea/output_root/run01028sub000leaktest.root /home/dsdaq/andrea/proto0data/run01028sub000.mid.lz4 --mt -- --pedestal 150 --MAF 100 --sigma 1.1`

## HEAP SUMMARY:
    in use at exit: 43,940,262 bytes in 72,604 blocks
  total heap usage: 2,438,777 allocs, 2,366,173 frees, 13,047,424,405 bytes allocated

## LEAK SUMMARY:
   definitely lost: 0 bytes in 0 blocks
   indirectly lost: 0 bytes in 0 blocks
     possibly lost: 0 bytes in 0 blocks
   still reachable: 1,010,685 bytes in 11,275 blocks
                      of which reachable via heuristic:
                        newarray           : 31,992 bytes in 49 blocks
                        multipleinheritance: 14,528 bytes in 3 blocks
        suppressed: 42,929,577 bytes in 61,329 blocks


# Run test

`dsanana.exe -O~/andrea/output_root/dsanaR1028.root $PROTO0DATA/run01028sub*.mid.lz4 --mt -- --pedestal 150 --MAF 100 --sigma 1.1`

run: 1028 events: 99836
Total number of Pulses: 2348658

## Module average processing time

|Module|Entries ST|Mean(ms) ST|RMS(ms) ST|Max(ms) ST|Sum(s) ST|Entries MT|Mean(ms) MT|RMS(ms) MT|Max(ms) MT|Sum(s) MT|
|------|:-------|-------:|-------:|-------:|-------:|:-------|-------:|-------:|-------:|-------:|
|UnpackAdc                	|100300	|0.4	|0.0	|55.5	|37.462	|100300	|0.0	|0.0	|0.4	|0.010 |
|Baseline                 	|100300	|0.0	|0.0	|0.7	|0.010	|100300	|0.7	|0.0	|35.7	|67.418|
|Filter                   	|100300	|0.0	|0.0	|0.4	|0.006	|99836	|0.4	|0.0	|45.8	|40.983|
|PulseFinder              	|100300	|0.0	|0.0	|0.2	|0.005	|99836	|0.3	|0.0	|52.2	|28.842|
|Histo                    	|100300	|0.0	|0.0	|0.3	|0.004	|99836	|0.1	|0.0	|25.1	|7.612 |
|TreeWriter               	|100300	|0.0	|0.0	|0.5	|0.006	|99836	|0.0	|0.0	|329.3	|4.066 |

./dsanana.exe	CPU time: 163.66s	User time: 74.80s	Average CPU Usage: ~218.8%

Each event has 28 channels. Each channel has 3000 samples. Each sample has 14 bit. Data processing rate: ~196 MB/s


