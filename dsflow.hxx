/******************************************************************
 * Flow Object(s) for DS analysis *
 * 
 * A. Capra
 * August 2021
 *
******************************************************************/

#ifndef __DSFLOW__
#define __DSFLOW__

#include "manalyzer.h"

#include <map>
#include <vector>
#include <string>
#include <stdexcept>

#include "TRawChannel.hxx"

class DSAdcEventFlow: public TAFlowEvent
{
public:
  const double fmidas_ts;
  std::map<int, unsigned int> fEventCounters; // Board ID -> event counter
  std::map<int, double> fTriggerTime; // Board ID -> (extended) trigger time tag
  std::vector<std::string> ftype;
  
private:
  std::vector<TRawChannel> fChannels;
  int fNchannels;
  const int fEventNumber;
  
public:
   DSAdcEventFlow(TAFlowEvent* flow,
                  double ts, int event_number):TAFlowEvent(flow),
                                               fmidas_ts(ts),fNchannels(0),
                                               fEventNumber(event_number)
   {}

  ~DSAdcEventFlow()
   {
      fEventCounters.clear();
      fTriggerTime.clear();
      fChannels.clear();
   }

  void add(TRawChannel* ch)
  {
     fChannels.push_back(*ch);
     ++fNchannels;
     //ch->Print();
  }

   void add(TRawChannel* ch, const char* type)
  {
     fChannels.push_back(*ch);
     ftype.emplace_back(type);
     ++fNchannels;
     //ch->Print();
  }

  const TRawChannel* getChannel(int ch) const
  {
    if( ch >= fNchannels )
      throw std::out_of_range("out of range!");
	
    if( fChannels.empty() )
      throw std::out_of_range("empty!"); 
      
    return &fChannels.at(ch);
  }

  inline int getNchannels() const
  {
    return fNchannels;
  }

  inline int getNsamples(int ch) const
  {
    return (int) fChannels.at(ch).GetNSamples();
  }

  inline int getSample(int ch, int s) const
  {
    return fChannels.at(ch).GetADCSample(s);
  }

  inline int getBoard(int ch) const
  {
    return fChannels.at(ch).GetBoardNumber();
  }

  inline int getChannelNumber(int ch) const
  {
     return fChannels.at(ch).GetChannelNumber();
  }

  inline int getChannelIndex(int ch) const
  {
     return fChannels.at(ch).GetIndex();
  }

  inline const std::vector<double>* getVector(int ch) const
  {
      return fChannels.at(ch).GetMeasurement();
  }

  inline const double* getWaveform(int ch) const
  {
     return fChannels.at(ch).GetWaveform();
  }

  inline int getEventNumber() const
  {
     return fEventNumber;
  }
};

#include "dsdata.hxx"

class DSProcessorFlow: public TAFlowEvent
{
public:
  std::vector<TDSPulse>* pulses;
  std::vector<TDSChannel>* channels;
  std::map<std::string,ADCconf> conf;
  int fEventNumber;

public:
  DSProcessorFlow(TAFlowEvent* flow):TAFlowEvent(flow)
  {
    pulses = new std::vector<TDSPulse>();
    channels = new std::vector<TDSChannel>();
    conf["V1725"]=ADCconf("V1725");
    conf["VX2740"]=ADCconf("VX2740");
  }
  DSProcessorFlow(TAFlowEvent* flow,int size):TAFlowEvent(flow)
  {
    pulses = new std::vector<TDSPulse>(size);
    channels = new std::vector<TDSChannel>(size);
  }

  ~DSProcessorFlow()
  {
    pulses->clear();
    if(pulses) delete pulses;
    channels->clear();
    if(channels) delete channels;
  }

  inline int GetNumberOfChannels() const { return (int) channels->size(); }
  inline TDSChannel* GetDSchan(int i) { return &channels->at(i); }
  inline int GetNumberOfPulses() const   { return (int) pulses->size();   }
  inline const TDSPulse* GetDSpulse(int i) const { return &pulses->at(i); }

};

#endif

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
