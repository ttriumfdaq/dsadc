/******************************************************************
 *  Basic Filerting *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <cassert>
#include <numeric>      // std::accumulate

class Filter
{
public:
   Filter()
   {}

   virtual std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      Reset(0);
      std::for_each(adc_samples->begin(),adc_samples->end(),
                    [this](double& x) { hit.push_back(x); } );
      return &hit;
   }

public:
   std::vector<double> hit;

   void Reset(int nsamples)
   {
      hit.resize(nsamples);
   }
};

class MAF: public Filter
{
public:
   MAF():Filter(),w(1){} // null filter

   MAF(int k):Filter(),w(k)
   {
      assert(w>0);
      fWindow=1./double(w);
   }

   std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      double sum=0.;
      for(int i=0; i<=w; ++i )
         {
            sum+=adc_samples->at(i);
            hit[i]=sum/double(i+1);
         }
      for( int i=w+1;i<n;++i )
         {
            hit[i]=hit[i-1]+(adc_samples->at(i)-adc_samples->at(i-w-1))*fWindow;
         }
      return &hit;
   }

private:
   int w;
   double fWindow;
};

class EXF: public Filter
{
public:
   EXF():Filter(),w(1),w1(0){} //null filter
   
   EXF(double a):Filter()
   {
      assert((a>0.) && (a<1.));
      w=a;
      w1=1.-w;
   }

   virtual std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      hit[0]=adc_samples->at(0);
      for(int i=1;i<n;++i)
         {
            hit[i]=w*adc_samples->at(i)+w1*hit[i-1];
         }
      return &hit;
   }

private:
   double w;
   double w1;
};


// this is an experimental filter
class EnF: public Filter
{
public:
   EnF():Filter(),m(0) {} //null filter
   
   EnF(double a,int k):Filter(),m(k)
   { 
      assert((a>0.) && (a<1.));
      w=a;
      w1=1.-w;
   }

   std::vector<double>* operator()(std::vector<double>* adc_samples)
   {
      int n=adc_samples->size();
      Reset(n);
      for(int i=0;i<m;++i)
         hit[i]=adc_samples->at(i);
      for(int i=m;i<n;++i)
         {
            hit[i]=w*adc_samples->at(i)+w1*hit[i-m];
         }
      return &hit;
   }

private:
   double w;
   double w1;
   int m;
};

class ARF: public Filter
{
public:
  ARF():Filter(),ftau(0.)
  {
    FixParameters();
  }
   
  ARF(double t):Filter(),ftau(t)
  {
    FixParameters();
  }

  std::vector<double>* operator()(const std::vector<double>* adc_samples)
  {
    Reset(adc_samples->size());

    // prepare AR kernel
    size_t nsamples=adc_samples->size();
    AR.resize(nsamples);
    AR[0]=adc_samples->back();
      
    size_t i;
    // Auto-Recursive filtering for the exponential tail
    for( i=1; i<nsamples; ++i)
      {
         AR[i] = adc_samples->at( nsamples-1-i ) + _a*AR[i - 1];
      }
    
    // create output
    for(i = 0; i<nsamples; ++i)
      {
         hit[i]= AR[nsamples-1-i] * _tau ;
      }
    return &hit;
  }

private:
  void FixParameters()
  {
    _tau =  1./ftau;
    _a = 1. - _tau;
  }

  
private:
  // filter parameters
  double ftau;

  // convenience variables
  double _a;
  double _tau;

  // Auto-Recursive vector
  std::vector<double> AR;
};

class ARMA: public Filter
{
public:
  ARMA():Filter(),ftau(0.),fsigma(0.),ffp_exp_ratio(0.)
  {
    kgsize=0;
    FixParameters();
  }
   
  ARMA(double t, double s, double k):Filter(),
                                     ftau(t),fsigma(s),ffp_exp_ratio(k)
  {
    gen_gaus_kernel();
    if( !(kgsize > 0) ) std::cerr<<"ARMA ctor: something wrong with the Gaussian kernel"<<std::endl;
    FixParameters();
  }

  std::vector<double>* operator()(const std::vector<double>* adc_samples)
  {
    Reset(adc_samples->size());
 
    // prepare filter kernels
    size_t nsamples=adc_samples->size();
    MA.resize(nsamples);
    AR.resize(nsamples);
    AR[0]=adc_samples->back();
      
    size_t i, j;
    double tmp;
    for( i=0; i<nsamples; ++i)
      {
	if( (int(i - ceiled) >= 0) && (i + halfsize < nsamples) )  // take care of waveform edges
	  {
	    tmp = 0.; // zero partial summation 
	    for(j=0; j<kgsize; ++j) // cross-correlation filtering for the fast peak
	      {
		tmp += adc_samples->at( i + halfsize - j ) * gker[j];
	      }
	    MA[i] = tmp; 
	  }
	else
	  MA[i] = 0.;

	// Auto-Recursive filtering for the exponential tail
	if( i ) AR[i] = adc_samples->at(nsamples-1-i) + _a*AR[i - 1];
      }

    // ARMA: put together
    for(i = 0; i<nsamples; ++i)
      {
	hit[i] = _scale_factor * AR[nsamples-1-i] * _tau  + scale_factor * MA[i] ;
      }
    return &hit;
  }

  virtual inline void Reset(size_t nsamples)
  {
    if( nsamples <= kgsize )
      std::cerr<<"ARMA Reset: Number of samples: "<<nsamples
	       <<" smaller than the Gaussian kernel size: "<<kgsize<<std::endl;
    hit.resize(nsamples);
  }

  inline void SetScaleFactor(double f) { scale_factor = f; _scale_factor = 1. - scale_factor; }


private:
  void gen_gaus_kernel()
  {
    gker.clear();
    kgsize = static_cast<int>(8*fsigma);
    for(size_t i=0; i<kgsize; ++i)
      gker.push_back(exp( -0.5*pow( (i-4.*fsigma)/fsigma,2) ) / (fsigma*sqrt(2*M_PI)) );
  }
  
  void FixParameters()
  {
    ceiled = ceil(kgsize / 2 ) + 1;
    halfsize = kgsize / 2;
    
    scale_factor = ffp_exp_ratio/ftau*(sqrt(2*M_PI)*fsigma);
    _scale_factor = 1. - scale_factor;
    _tau =  1./ftau;
    _a = 1. - _tau;
  }

  
private:
  // filter parameters
  double ftau; // in samples
  double fsigma; // in samples
  double ffp_exp_ratio;

  // convenience variables
  double _a;
  double _tau; // 1/tau : 1/samples

  // combine (1-scale_factor)*AR+scale_factor*MA
  double scale_factor;
  double _scale_factor;

  // gaussian kernel vector
  std::vector<double> gker;
  size_t kgsize;
  size_t ceiled;
  size_t halfsize;

  // finite response vector
  std::vector<double> MA;
  // Auto-Recursive vector
  std::vector<double> AR;
};

class FIR: public Filter
{
public:
   FIR():Filter(),fNcoeff(0),fNorm(1.){}

   FIR(unsigned int n, double* c):Filter(),fNcoeff(n),fCoeff(c,c+n/sizeof(double) )
   {
      fNorm=0.;
      for(unsigned i=0; i<n; ++n) fNorm+=c[i];
   }

   FIR(std::vector<double> c):Filter(),fCoeff(c)
   {
      fNcoeff=fCoeff.size();
      fNorm=std::accumulate(c.begin(),c.end(),double(0));
   }

   FIR(std::string fname,bool norm=false):Filter(),fNorm(1.) // Cross-Correlation Filter
   {
      if( fname != "" )
         CoeffLoader(fname);
      else
         CoeffLoader();
      fNcoeff=fCoeff.size();
      if( norm ) fNorm=static_cast<double>(fNcoeff);
   }

   virtual std::vector<double>* operator()(const std::vector<double>* adc_samples)
   {
      size_t n=adc_samples->size();
      Reset(n);
      double sum=0.;
      for(size_t i=0;i<=fNcoeff;++i)
         {
            sum+=adc_samples->at(i);
            hit[i]=sum/double(i+1);
         }
 
      auto it=adc_samples->begin();
      for(size_t i=fNcoeff+1;i<n;++i)
         {
            hit[i]=std::inner_product(fCoeff.begin(),fCoeff.end(),it,double(0))/fNorm;
            ++it;
         }
      
      return &hit;
   }

   void SetODBcoefficients(std::vector<int> c)
   {
      fCoeff.clear();
      for( auto& a : c )
         {
            fCoeff.push_back( static_cast<double>(a) );
         }
      fNcoeff=fCoeff.size();
      fNorm=std::accumulate(fCoeff.begin(),fCoeff.end(),double(0));
   }

   void Print()
   {
      std::cout<<"FIR coefficients: ";
      for( auto& a : fCoeff )
         {
            std::cout<<a<<" ";
         }
      std::cout<<"\n";
   }

   int GetNumberOfCoefficients() const { return fNcoeff; }

private:
   void CoeffLoader(std::string fname="../dsadc/templates/Normalized_Template_CH0_PDUplus.dat")
   {
      std::ifstream fin(fname);
      assert(fin.is_open());
      std::cout<<"FIR template file: "<<fname<<std::endl;
      double y;
      while(fin>>y)
         {
            fCoeff.push_back(y);
         }
      fin.close();
   }

private:
   size_t fNcoeff;
   std::vector<double> fCoeff;
   double fNorm;
};



#include "json.hpp"
using json = nlohmann::json;


class FilterFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};
  

class FilterModule: public TARunObject
{
public:
   FilterFlags* fFlags;

private:
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encountered errors

   int fWindow=100;
   double fSmoothing=0.88;
   bool fMAF=false;
   bool fEXF=false;

   // experimental feature
   int fMemory=3;
   bool fEnF=false;

   // ARMA filter, see eulero.hxx
   bool fARMA=false;
   double fARMAtau=80.;
   double fARMAsigma=2.;
   double fARMAexp=1.;

   // AR filter
   bool fARF=false;

   // 'Generic' FIR
   bool fFIR=false;
   std::vector<double> fCoeff;

   // Cross-Correlation filter
   bool fCCF=false;
   std::string fTemplateFile;


   // Filter classes
   MAF avgfilter;
   EXF expfilter;
   EnF testfilter;
   ARMA armafilter;
   ARF arfilter;
   FIR firfilter; 
   FIR xcorrfilter;

   // Filtered waveform baseline
   int fPedestal;

   std::vector<int> fFilteredChannels;

public:
   FilterModule(TARunInfo* runinfo, FilterFlags* f): TARunObject(runinfo),
                                                     fFlags(f), fCounter(0), fError(0),
                                                     fPedestal(100)
   {
      if(fFlags->fVerbose) std::cout<<"FilterModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="Filter";
#endif
      
      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"FilterModule Json parsing success!"<<std::endl;
      fin.close();

      try
         {
            fPedestal=settings["Baseline"]["Pedestal"].get<int>();
         }
      catch(json::out_of_range& e) 
         {
            std::cerr<<e.what()<<std::endl;
         }

      std::string filter_name = settings["Filter"]["Name"].get<std::string>();
      if( filter_name == "Moving Average" )
         {
            fMAF=true;
            fWindow=settings["Filter"]["Par0"].get<int>();
            std::cout<<filter_name<<" window: "<<fWindow<<std::endl;
            avgfilter = MAF( fWindow );
         }
      else if( filter_name == "Exponential" )
         {
            fEXF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<std::endl;
            expfilter = EXF( fSmoothing );
         }
      else if( filter_name == "Exponential with Memory" )
         {
            fEnF=true;
            fSmoothing=settings["Filter"]["Par0"].get<double>();
            fMemory=settings["Filter"]["Par1"].get<int>();
            std::cout<<filter_name<<" smoothing: "<<fSmoothing<<" memory: "<<fMemory<<std::endl;
            testfilter = EnF( fSmoothing, fMemory );
         }
      else if( filter_name == "ARMA" )
         {
            fARMA=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            fARMAsigma=settings["Filter"]["Par1"].get<double>();
            fARMAexp=settings["Filter"]["Par2"].get<double>();
            std::cout<<filter_name<<" tau: "<<fARMAtau
                     <<" sigma_fp: "<<fARMAsigma<<" Afp_Aexp_ratio: "<<fARMAexp<<std::endl;
            armafilter = ARMA( fARMAtau, fARMAsigma, fARMAexp );
         }
      else if( filter_name == "AR" )
         {
            fARF=true;
            fARMAtau=settings["Filter"]["Par0"].get<double>();
            std::cout<<filter_name<<" filter tau: "<<fARMAtau<<std::endl;
            arfilter = ARF( fARMAtau );
         }
      else if( filter_name == "FIR" )
         {
            fFIR=true;
            firfilter = FIR();
            std::cout<<filter_name<<" filter with coefficients from ODB"<<std::endl;
         }
      else if( filter_name == "Cross-Correlation" )
         {
            fCCF=true; 
            
            try {
               fTemplateFile=settings["Filter"]["Par0"].get<std::string>();
               xcorrfilter=FIR(fTemplateFile);
            }
            catch(json::type_error& e) {
               std::cerr<<e.what()<<" couldn't find path to template file, trying with sequence"<<std::endl;            
            
               try {
                  fCoeff=settings["Filter"]["Par0"].get<std::vector<double>>();
               }
               catch(json::type_error& e) {
                  std::cerr<<e.what()<<" coeffiecient sequence not available"<<std::endl;
               }

               if( fCoeff.empty() )
                  fCoeff.assign(100,1);
               else if( fCoeff.size() == 1 )
                  fCoeff.assign(fCoeff[0],1);

               xcorrfilter=FIR(fCoeff);

            }


            std::cout<<filter_name<<" filter with "<<xcorrfilter.GetNumberOfCoefficients()<<" coefficients"<<std::endl;
         }
      else
         {
            std::cerr<<"Unkwown filter "<<filter_name<<std::endl;
         }

      try
         {
            fFilteredChannels=settings["Filter"]["Skip"].get<std::vector<int>>();
            std::cout<<"FilterModule::ctor Already filtered channels:";
            for(auto& ch: fFilteredChannels) std::cout<<" "<<ch;
            std::cout<<"\n";
         }
      catch(json::type_error& e) 
         {
            std::cerr<<"FilterModule::ctor reading skip channels: "<<e.what()<<std::endl;
         }
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"FilterModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
      if( fFIR )
         {
            std::vector<int> a;
            runinfo->fOdb->RIA("/VX2740 defaults/User registers/FIR filter coefficients",&a);
            firfilter.SetODBcoefficients( a );
            firfilter.Print();
         }
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"FilterModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"FilterModule::AnalyzeFlowEvent : "<<wf_flow->GetNumberOfChannels()<<std::endl;
      
      double baseline, baseline_rms;
      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);
            if( fFlags->fVerbose && 0 ) dsch->Print();
            int shift=0;
            if( IsFiltered( dsch ) ) 
               {
                  if( fFlags->fVerbose )
                     std::cout<<"FilterModule::AnalyzeFlowEvent Skipping selected channel "
                              <<dsch->index<<" at "<<i<<std::endl;
                  dsch->filtered_wf = std::vector<double>(dsch->unfiltered_wf);
               }
            else shift=Filter( dsch );

            baseline=std::accumulate(dsch->filtered_wf.begin()+shift,
                                     dsch->filtered_wf.begin()+shift+fPedestal,
                                            double(0))/(double) fPedestal;
            baseline_rms=std::inner_product(dsch->filtered_wf.begin()+shift,
                                            dsch->filtered_wf.begin()+shift+fPedestal,
                                            dsch->filtered_wf.begin()+shift,double(0));
            baseline_rms = sqrt( (baseline_rms / (double) fPedestal)
                                 - (baseline * baseline) );
            
            dsch->filter_baseline = baseline;
            dsch->filter_rms = baseline_rms;

            // calculate the standard deviation of the whole waveform
            // using the mean baseline of the fist nbase_samples
            double number_of_samples = static_cast<double>( dsch->filtered_wf.size() - shift );
            double wf_rms=std::inner_product(dsch->filtered_wf.begin()+shift,
                                             dsch->filtered_wf.end(),
                                             dsch->filtered_wf.begin(),double(0));
            double bline=dsch->filter_baseline;
            wf_rms=sqrt( (wf_rms / number_of_samples) - (bline*bline) );
            dsch->filterwf_rms = wf_rms;

            if( fFlags->fVerbose )
               std::cout<<"FilterModule:: ch: "<<dsch->index
                        <<" wf rms: "<<wf_rms
                        <<"\tflt baseline: "<<baseline
                        <<" RMS: "<<baseline_rms<<std::endl;
         }
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }

   int Filter(TDSChannel* dsch)
   {
      int shift=0;
      if( fMAF )
         {
            dsch->filtered_wf = std::vector<double>(*avgfilter( &dsch->unfiltered_wf ));
            shift=fWindow;
         }
      else if( fEXF )
         {
            dsch->filtered_wf = std::vector<double>(*expfilter( &dsch->unfiltered_wf ));
            shift=1;
         }
      else if( fEnF )
         {
            dsch->filtered_wf = std::vector<double>(*testfilter( &dsch->unfiltered_wf ));
            shift=fMemory;
         }
      else if( fARMA )
         {
            dsch->filtered_wf = std::vector<double>(*armafilter( &dsch->unfiltered_wf ));
         }
      else if( fARF )
         {
            dsch->filtered_wf = std::vector<double>(*arfilter( &dsch->unfiltered_wf ));
         }
      else if( fFIR )
         {
            dsch->filtered_wf = std::vector<double>(*firfilter( &dsch->unfiltered_wf ));
            shift=firfilter.GetNumberOfCoefficients();
         }
      else if( fCCF )
         {
            dsch->filtered_wf = std::vector<double>(*xcorrfilter( &dsch->unfiltered_wf ));
            shift=xcorrfilter.GetNumberOfCoefficients();
         }
      else
         {
            if( fFlags->fVerbose )
               std::cout<<"FilterModule::AnalyzeFlowEvent WARNING: the filtered waveform will be unfiltered"<<std::endl;
            dsch->filtered_wf = std::vector<double>(dsch->unfiltered_wf);
         }
      return shift;
   }

   bool IsFiltered(TDSChannel* dsch)
   {
      for( auto& ich: fFilteredChannels )
         {
            if( ich == dsch->index ) return true;
         }
      return false;
   }

};


class FilterModuleFactory: public TAFactory
{
public:
   FilterFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("FilterModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
      
   }
   
   void Finish()
   {
      printf("FilterModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("FilterModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new FilterModule(runinfo, &fFlags);
   }

};

static TARegister tar __attribute__((init_priority(105))) (new FilterModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
