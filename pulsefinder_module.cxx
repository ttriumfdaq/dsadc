/******************************************************************
 *  Hit finder *
 * 
 * A. Capra
 * August 2021
 *
 ******************************************************************/

#include "manalyzer.h"
#include "midasio.h"

#include "dsflow.hxx"
#include "dsdata.hxx"

#include <iostream>
#include <fstream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <iterator>
#include <numeric>
#include <cassert>

class PulseFinder
{
private:
   int fNpulses;
   double fThreshold;
   std::vector<int> fPulseStart;
   std::vector<int> fPulseEnd;
   std::vector<int> fPeakTime;
   std::vector<double> fAveragePeakTime;
   std::vector<double> fAmplitude;
   std::vector<double> fCharge;
   std::vector<int> fDuration;
   int fRejectShort;
   int fIntBefore;
   int fIntAfter;
   int fIntTotal;
   int fTimeAverageBefore;
   int fTimeAverageAfter;
   int fTimeAverageTotal;
   
public:
   PulseFinder():fNpulses(0),fThreshold(0),
                 fRejectShort(1),
                 fIntBefore(100),fIntTotal(250),
                 fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }
   PulseFinder(double& s, double& n):fNpulses(0),fThreshold(s*n),
                                     fRejectShort(1),
                                     fIntBefore(100),fIntTotal(250),
                                     fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }
   PulseFinder(double& t):fNpulses(0),fThreshold(t),
                          fRejectShort(1),
                          fIntBefore(100),fIntTotal(250),
                          fTimeAverageBefore(5),fTimeAverageAfter(6)
   {
      fIntAfter=fIntTotal-fIntBefore;
      fTimeAverageTotal=fTimeAverageBefore+fTimeAverageAfter;
   }

   inline int GetStartTime(int i) const { return fPulseStart.at(i); }
   inline int GetPeakTime(int i) const { return fPeakTime.at(i); }
   inline double GetAveragePeakTime(int i) const { return fAveragePeakTime.at(i); }
   inline double GetAmplitude(int i) const { return fAmplitude.at(i); }
   inline double GetCharge(int i) const { return fCharge.at(i); }
   inline int GetTimeOverThreshold(int i) const { return fDuration.at(i); }

   inline void SetThreshold(double t) { fThreshold=t; }
   inline void SetMinimumDuration(int d) { fRejectShort=d; }
   inline void SetIntegrationWindow(int before, int tot)
   {
      fIntBefore=before;fIntTotal=tot;
      fIntAfter=fIntTotal-fIntBefore;
      assert(fIntAfter>0);
   }

   inline void Reset()
   {
      fNpulses=0;
      fPulseStart.clear();
      fPulseEnd.clear();
      fPeakTime.clear();
      fAveragePeakTime.clear();
      fAmplitude.clear();
      fCharge.clear();
      fDuration.clear();
   }
   
   int operator()(const std::vector<double>* adc_samples)
   {
      std::vector<double> abs_adc_samples(adc_samples->size());
      // Here I assume that all the pulses are negative this to
      // simplify the algorithm, I flip the waveform
      // One could capture a parameter in the lambda-function
      // to make change this at, e.g., runtime.
      std::transform(adc_samples->begin(),adc_samples->end(),
                     abs_adc_samples.begin(),
                     [](double y){return fabs(y);});
      
      std::vector<double>::iterator it = abs_adc_samples.begin();
      const double thr=fThreshold;
      while(true)
         {
            if( it>=abs_adc_samples.end() ) break;

            // find where waveform is above thr
            it = std::find_if( it, abs_adc_samples.end(),
                               [thr](double const& r)
                               { return (r>thr); } );
            // if it's never above thr, quit
            if( it==abs_adc_samples.end() ) break;
            int iStart = (int) std::distance(abs_adc_samples.begin(),it);
            fPulseStart.push_back( iStart );
            // std::cout<<"\t"<<iStart;

            // find where waveform goes back below thr
            it = std::find_if( it, abs_adc_samples.end(),
                               [thr](double const& r)
                               { return (r<thr); } );
            int iEnd = (int) std::distance(abs_adc_samples.begin(),it);
            // std::cout<<" "<<iEnd;

            // if the pulse is too short, might just a fluke
            // skip
            int duration = iEnd-iStart;
            if( duration < fRejectShort )
               {
                  fPulseStart.pop_back();
                  ++it;
                  //  std::cout<<" too short"<<std::endl;
                  continue;
               }
            fPulseEnd.push_back(iEnd);
            fDuration.push_back(duration);

            // find peak pulse
            std::vector<double>::iterator max_pos = std::max_element(abs_adc_samples.begin()+iStart,
                                                                     abs_adc_samples.begin()+iEnd);
            int max_timebin=(int)std::distance(abs_adc_samples.begin(),max_pos);
            // std::cout<<" "<<max_timebin<<std::endl;
            fPeakTime.push_back(max_timebin);
            double a = *max_pos; fAmplitude.push_back(a);
            // std::cout<<" "<<a;

            double t0 = (double) max_timebin;
            if( (max_timebin+fTimeAverageAfter) < (int) abs_adc_samples.size() &&
                (max_timebin-fTimeAverageBefore) >= 0 )
               {
                  std::vector<double> time( fTimeAverageTotal ); // prepare time axis
                  std::iota(time.begin(),time.end(),
                            max_timebin-fTimeAverageBefore); // fill-in the timebin
                  double w=std::accumulate(max_pos-fTimeAverageBefore, 
                                           max_pos+fTimeAverageAfter,double(0));
                  // weighted average of time
                  t0 = std::inner_product( max_pos-fTimeAverageBefore, 
                                           max_pos+fTimeAverageAfter,
                                           time.begin(), double(0) ) / w;
               }
            fAveragePeakTime.push_back(t0);
            
            // charge integration over a fixed range
            // around the pulse peak
            int maxn=(int)std::distance(abs_adc_samples.begin(),max_pos);
            int bef=maxn-fIntBefore;
            if( bef<0 ) bef=0;
            int aft=maxn+fIntAfter;
            if( aft>int(abs_adc_samples.size()) )
               aft=int(abs_adc_samples.size());
            double q = std::accumulate(abs_adc_samples.begin()+bef,
                                       abs_adc_samples.begin()+aft,0.0);
            fCharge.push_back(q);
            //std::cout<<" "<<q<<std::endl;
            
            ++fNpulses;
            ++it;
         }
      return (int)fAmplitude.size();
   }

   void print(bool all=true) const
   {
      std::cout<<"Pulse Finder found "<<fNpulses
               <<" pulse(s) above threshold: "<<fThreshold<<std::endl;
      if( !all ) return;
      for(size_t i=0;i<fAmplitude.size();++i)
         {
            std::cout<<std::setw(3)<<i<<") at ["<<std::setw(6)<<fPulseStart.at(i)
                     <<","<<std::setw(6)<<fPulseEnd.at(i)
                     <<"] = "<<std::setw(4)<<fPulseEnd.at(i)-fPulseStart.at(i)
                     <<" samples\tamplitude: "<<std::setw(6)<<fAmplitude.at(i)
                     <<" charge: "<<std::setw(6)<<fCharge.at(i)
                     <<" Time over Thr: "<<std::setw(6)<<fDuration.at(i)<<" samples"<<std::endl;
         }
   }

};



#include "json.hpp"
using json = nlohmann::json;


class PulseFinderFlags
{
public:
   bool fVerbose = false;
   std::string fConfigName="master.json";
};


class PulseFinderModule: public TARunObject
{
public:
   PulseFinderFlags* fFlags;
   int fCounter; // counts the number of processed events
   int fError;   // counts the number of encoutered errors

private:
   int fNtotPulses;

   bool fFixed;
   double fAmplitude;
   double fNsigma;
   int fShortPulse;
   int fStartIntegration;
   int fTotalIntegration;
   std::string fTimingMode; //TBD

public:
   PulseFinderModule(TARunInfo* runinfo, PulseFinderFlags* f): TARunObject(runinfo),
                                                               fFlags(f), fCounter(0), fError(0),
                                                               fNtotPulses(0)
   {
      if(fFlags->fVerbose) std::cout<<"PulseFinderModule ctor"<<std::endl;
#ifdef HAVE_MANALYZER_PROFILER
      fModuleName="PulseFinder";
#endif

      std::ifstream fin(fFlags->fConfigName.c_str());
      json settings;
      fin>>settings;
      if(fFlags->fVerbose)
         std::cout<<"PulseFinderModule Json parsing success!"<<std::endl;
      fin.close();

      fFixed=settings["Pulse"]["Fixed"].get<bool>();
      fAmplitude=settings["Pulse"]["Amplitude"].get<double>();
      fNsigma=settings["Pulse"]["Sigma"].get<double>();
      fShortPulse=settings["Pulse"]["Duration"].get<int>();
      fStartIntegration=settings["Pulse"]["Charge"]["before"].get<int>();
      fTotalIntegration=settings["Pulse"]["Charge"]["total"].get<int>();
      
      fTimingMode="thr";
      //fTimingMode="";
      if( settings["Filter"]["Name"].get<std::string>() == "ARMA" )
         fTimingMode="avg";
      std::cout<<"PulseFinderModule:: timing mode "<<fTimingMode<<std::endl;
   }


   void BeginRun(TARunInfo* runinfo)
   {
      if(runinfo->fFileName.empty() )
         std::cout<<"PulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" online"<<std::endl;
      else
         std::cout<<"PulseFinderModule::BeginRun() run: "<<runinfo->fRunNo<<" midasfile: "<<runinfo->fFileName.c_str()<<std::endl;
   }

   void EndRun(TARunInfo* runinfo)
   {
      std::cout<<"PulseFinderModule::EndRun() run: "<<runinfo->fRunNo<<" events: "<<fCounter<<" errors: "<<fError<<" Total number of Pulses: "<<fNtotPulses<<std::endl;
   }

  
   TAFlowEvent* AnalyzeFlowEvent(TARunInfo* /*runinfo*/, TAFlags* flags, TAFlowEvent* flow)
   {
      DSProcessorFlow* wf_flow = flow->Find<DSProcessorFlow>();
      if( !wf_flow ) 
         {
            ++fError;
#ifdef HAVE_MANALYZER_PROFILER
            *flags|=TAFlag_SKIP_PROFILE;
#endif
            return flow;
         }

      if( fFlags->fVerbose )
         std::cout<<"PulseFinderModule::AnalyzeFlowEvent # of ch: "<<wf_flow->GetNumberOfChannels()<<std::endl;

      double threshold=fAmplitude;
      int npevt=0;
      for(int i=0; i<wf_flow->GetNumberOfChannels(); ++i)
         {
            TDSChannel* dsch = wf_flow->GetDSchan(i);
            std::vector<double>* wf;
            double rms=0.;
            if( dsch->filtered_wf.size() > 0 )
               {
                  wf=&dsch->filtered_wf;
                  rms=dsch->filter_rms;
               }
            else
               {
                  wf=&dsch->unfiltered_wf;
                  rms=dsch->baseline_rms;
               }
            
            if( !fFixed )
               threshold = rms * fNsigma;
            PulseFinder find( threshold );
            find.SetMinimumDuration( fShortPulse );
            find.SetIntegrationWindow( fStartIntegration, fTotalIntegration );
            int np = find(wf);
            if( fFlags->fVerbose && np )
               {
                  dsch->Print();
                  find.print(true);
               }

            for(int i=0; i<np; ++i)
               {
                  TDSPulse aPulse(dsch->module,dsch->channel,dsch->board.c_str());
                  aPulse.charge=find.GetCharge(i);
                  if( fTimingMode == "avg" )
                     aPulse.time=find.GetAveragePeakTime(i);
                  else if( fTimingMode == "thr" )
                     aPulse.time=(double)find.GetStartTime(i);
                  else
                     aPulse.time=(double)find.GetPeakTime(i);
                  aPulse.height=find.GetAmplitude(i);
                  aPulse.tot=(double)find.GetTimeOverThreshold(i);
                  if( fFlags->fVerbose && 0 ) aPulse.Print();
                  wf_flow->pulses->push_back(aPulse);
               }

            fNtotPulses+=np;
            npevt+=np;
         }

      if( fFlags->fVerbose )
         std::cout<<"PulseFinderModule::AnalyzeFlowEvent # of pulses: "<<npevt<<std::endl;
      
      flow=wf_flow;
      ++fCounter;
      return flow;
   }
};


class PulseFinderModuleFactory: public TAFactory
{
public:
   PulseFinderFlags fFlags;
   
public:
   void Init(const std::vector<std::string> &args)
   {
      printf("PulseFinderModuleFactory::Init!\n");
      
      for (unsigned i=0; i<args.size(); i++)
         {
            if( args[i] == "--verbose" ||
                args[i] == "-v" )
               fFlags.fVerbose = true;
            if( args[i] == "--config" ||
                args[i] == "--conf" ||
                args[i] == "-c" )
               fFlags.fConfigName=args[i+1];
         }
   }
   
   void Finish()
   {
      printf("PulseFinderModuleFactory::Finish!\n");
   }
   
   TARunObject* NewRunObject(TARunInfo* runinfo)
   {
      printf("PulseFinderModuleFactory::NewRunObject, run %d, file %s\n", runinfo->fRunNo, runinfo->fFileName.c_str());
      return new PulseFinderModule(runinfo, &fFlags);
   }

};

static TARegister tar __attribute__((init_priority(106))) (new PulseFinderModuleFactory);

/* emacs
 * Local Variables:
 * tab-width: 8
 * c-basic-offset: 3
 * indent-tabs-mode: nil
 * End:
 */
